<?php

require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require(dirname(__FILE__) . '/lib.php');

// Get params
$courseid = required_param('id', PARAM_INT);
$action   = optional_param('a', null, PARAM_ACTION);

// Module unavailable for course id 0 or 1
if ($courseid == 1 || !$course = $DB->get_record('course', array('id' => $courseid)))
    print_error('invalidcourse');

// Security and access check.
require_course_login($courseid);
$context = \context_course::instance($courseid);
require_capability('moodle/course:update', $context, null, true, 'errormustbeteacher', 'block_cegep');

// Verify if external database enrolment is enabled
if (!in_array('database',explode(',',$CFG->enrol_plugins_enabled)))
    print_error('errorenroldbnotavailable','block_cegep');

// Prepare external enrolment database connection
if ($enroldb = enroldb_connect()) {
    $enroldb->Execute("SET NAMES 'utf8'");
}
else {
    error_log('[ENROL_DB] Could not make a connection');
    print_error('dbconnectionfailed','error');
}

// Prepare external SIS database connection
if ($sisdb = sisdb_connect()) {
    $sisdb->Execute("SET NAMES 'utf8'");
}
else {
    error_log('[SIS_DB] Could not make a connection');
    print_error('dbconnectionfailed','error');
}

// Set up and display page header
$strtitle = get_string('enrolment','block_cegep');
$PAGE->set_pagelayout('incourse');
$PAGE->set_url(new moodle_url($CFG->wwwroot . '/blocks/cegep/block_cegep_enrolment.php', array('id' => $courseid, 'a' => $action)));
$PAGE->set_title($course->fullname . ': ' . $strtitle);
$PAGE->set_heading($course->fullname . ': ' . $strtitle);

// Load renderer class
$renderer = $PAGE->get_renderer('block_cegep');

// Get system context for global Admin CEGEP permissions
$syscontext = \context_system::instance();

// Main switch
switch ($action) {
    case 'enrol' :
        if (is_siteadmin() || has_capability('block/cegep:enroladmin_course', $syscontext))
            echo $renderer->page_enrol_admin();
        else
            echo $renderer->page_enrol();
        break;
   case 'unenrol' :
        echo $renderer->page_unenrol();
        break;
     case 'enrolprogram' :
        require_capability('block/cegep:enroladmin_program', $syscontext);
        echo $renderer->page_enrolprogram();
        break;
    case 'unenrolprogram' :
        require_capability('block/cegep:enroladmin_program', $syscontext);
        if ($num_programenrolments > 0)
            echo $renderer->page_unenrolprogram();
        else
            notify(get_string('noprogramsenrolled','block_cegep'));
        break;
    default :
        echo $renderer->page_studentlist();
        break;
}

$enroldb->Close();
$sisdb->Close();

exit;
