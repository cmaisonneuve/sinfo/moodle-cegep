<?php

require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require(dirname(__FILE__) . '/lib.php');

// Get params
$coursecode = optional_param('coursecode', null, PARAM_ALPHANUM);

// Security and access check
$PAGE->set_context(context_system::instance());
require_login();

// Set up and display page header
$strtitle = get_string('enrolment','block_cegep');
$PAGE->set_pagelayout('mydashboard');
$PAGE->set_url(new moodle_url($CFG->wwwroot . '/blocks/cegep/block_cegep_createcourse.php', array('coursecode' => $coursecode)));
$PAGE->set_heading('heading');
$PAGE->set_title(get_string('coursecreate','block_cegep'));

// Load renderer class
$renderer = $PAGE->get_renderer('block_cegep');

// Render the form
echo $renderer->page_createcourse($coursecode);

?>
