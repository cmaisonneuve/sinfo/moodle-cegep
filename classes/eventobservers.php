<?php

namespace block_cegep;

defined('MOODLE_INTERNAL') || die();

class eventobservers {
    public static function course_deleted(\core\event\course_deleted $event) {
        $course = new \stdClass();
        $course->idnumber = $event->other['idnumber'];
        cegep_delete_course_enrolments($course);
    }
}
