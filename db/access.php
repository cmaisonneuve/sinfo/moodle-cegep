<?php

defined('MOODLE_INTERNAL') || die();

$capabilities = array(
    'block/cegep:addinstance' => array(
        'captype' => 'write',
        'contextlevel' => CONTEXT_BLOCK,
        'archetypes' => array(
            'editingteacher' => CAP_ALLOW,
            'manager' => CAP_ALLOW
        ),
        'clonepermissionsfrom' => 'moodle/site:manageblocks'
    ),
    'block/cegep:enroladmin_course' => array(
        'captype'      => 'write',
        'contextlevel' => CONTEXT_SYSTEM,
    ),
    'block/cegep:enroladmin_program' => array(
        'captype'      => 'write',
        'contextlevel' => CONTEXT_SYSTEM,
    )
);

?>

